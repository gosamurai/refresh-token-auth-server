FROM  openjdk:8-jre-alpine
ADD target/refresh-token-auth-server-*.jar /root
ENTRYPOINT java -jar /root/refresh-token-auth-server-*.jar  --server.port=8085
EXPOSE 8085
